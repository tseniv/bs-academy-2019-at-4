const DeleteListPage = require('./deleteList_po');
const page = new DeleteListPage();

class DeleteListActions {
  moveToMenu() {
    page.dropdown.waitForDisplayed(10000);
    page.dropdown.moveTo();
  }

  navigateToLists() {
    this.moveToMenu();
    page.lists.waitForDisplayed(2000);
    page.lists.click();
  }

  deleteFirstList() {
    page.deleteButton.waitForDisplayed(2000);
    page.deleteButton.click();
  }

  confirmListDelete() {
    page.confirmDeleteButton.waitForDisplayed(2000);
    page.confirmDeleteButton.click();
  }

  getFirstListTitle() {
    return page.firstListTitle.getText();
  }
}

module.exports = DeleteListActions;
