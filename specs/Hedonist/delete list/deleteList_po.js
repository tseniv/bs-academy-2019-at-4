class DeleteListPage {

  get dropdown() { return $('div.profile'); }
  get lists() { return $('a[href="/my-lists"]'); }
  get deleteButton() { return $('.place-item__actions .button.is-danger'); }
  get confirmDeleteButton() { return $('.animation-content.modal-content .button.is-danger'); }
  get firstListTitle() { return $('h3[class="title has-text-primary"]'); }
  
}

module.exports = DeleteListPage;
