const LogInPage = require('./logIn_po');
const page = new LogInPage();

class LogInActions {
  enterEmail(value) {
    page.emailInput.waitForDisplayed(2000);
    page.emailInput.setValue(value);
  }

  enterPassword(value) {
    page.newPasswordInput.waitForDisplayed(2000);
    page.newPasswordInput.setValue(value);
  }

  loginButton() {
    page.loginButton.waitForDisplayed(2000);
    page.loginButton.click();
  }
}

module.exports = LogInActions;
