const CreateListPage = require('./createList_po');
const page = new CreateListPage();

class CreateListActions {
  moveToMenu() {
    page.dropdown.waitForDisplayed(10000);
    page.dropdown.moveTo();
  }

  navigateToLists() {
    this.moveToMenu();
    page.lists.waitForDisplayed(2000);
    page.lists.click();
  }

  addList() {
    page.addListButton.waitForDisplayed(2000);
    page.addListButton.click();
  }

  enterListName(value) {
    page.listNameInput.waitForDisplayed(2000);
    page.listNameInput.setValue(value);
  }

  saveChanges() {
    page.saveButton.waitForDisplayed(2000);
    page.saveButton.click();
  }

  getFirstListTitle() {
    return page.firstListTitle.getText();
  }

  clearField(value) {
    page.listNameInput.setValue(value);
  }

  updateButton() {
    page.updateButton.waitForDisplayed(2000);
    page.updateButton.click();
  }

}

module.exports = CreateListActions;
