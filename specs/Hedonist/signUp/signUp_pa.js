const SignUpPage = require('./signUp_po');
const page = new SignUpPage();

class SignUpActions {
  enterFirstName(value) {
    page.firstNameInput.setValue(value);
  }

  enterLastName(value) {
    page.lastNameInput.setValue(value);
  }

  enterEmail(value) {
    page.emailInput.setValue(value);
  }

  enterNewPassword(value) {
    page.newPasswordInput.setValue(value);
  }

  createUser() {
    page.createButton.click();
  }

  getInvalidEmailErrorMessage() {
    return page.invalidEmailField.getText();
  }
}

module.exports = SignUpActions;
