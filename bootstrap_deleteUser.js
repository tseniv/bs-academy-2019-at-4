const fetch = require('node-fetch');
const Helper = require('./specs/Hedonist/helpers/helpers')

{
    const url = 'http://165.227.137.250/api/v1';
    const args = require('./specs/hedonistTestData.json');
    

    class Client {
        static async sendDeleteUserRequest(url, args) {
            const token  = await Helper.getAuthToken();
            const user = await Helper.getUser();
            return fetch(`${url}/users/${user.id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            });
        }
    }
    
    async function deleteUser(url, args) {

        console.log(`Deleting user for ${url}`);
        const response = await Client.sendDeleteUserRequest(url, args);

        if (response.status === 200) {
            console.log(`User is deleted on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully delete user for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    }

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting User=========');
        deleteUser(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
