const fetch = require('node-fetch');
const Helper = require('./specs/Hedonist/helpers/helpers')

{
    const url = 'http://165.227.137.250/api/v1';
    const args = require('./specs/hedonistTestData.json');

    class List {
        static async sendCreateListRequest(url, args) {
            const token  = await Helper.getAuthToken();
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    name: args.name
                })
            });
        }
    }
    
    async function createList(url, args) {

        console.log(`Creating list for ${url}`);
        const response = await List.sendCreateListRequest(url, args);

        if (response.status === 201) {
            console.log(`List is created on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    }

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating List=========');
        createList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
